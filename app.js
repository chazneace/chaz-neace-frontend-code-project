const div = document.getElementById("boxes");
const addJob = document.getElementById("addJob");
const loading = document.getElementById("loading");
const months = ['January','Febuary','March','April','May','June','July','August','September','October','November','December']

setTimeout
setTimeout(function() {
  run();
  loading.style.display = "none";
}, 2000);

function run() {
  fetch('jobList.json')
    .then(res => res.json())
    .then(data => {

      var file = data;

      for (let index = 0; index < data.data.length; index++) {

        let day = `
        ${months[new Date(file.data[index].posted_datetime).getMonth()]} 
        ${new Date(file.data[index].posted_datetime).getDate()} 
        ${new Date(file.data[index].posted_datetime).getFullYear()}`

        div.innerHTML += 
        `<ul class="jobPanel">
          <li class="title">${file.data[index].title}</li>
          <li class="details">${file.data[index].business_name} - ${file.data[index].city} ${file.data[index].region} - ${day}</li>
          <li class="summary">${file.data[index].job_summary.slice(0,300)}</li>
          <li>...</li>
         </ul>`
      };

      addJob.addEventListener("click", () => {
        run()
      });
    })
    .catch(error => {
      console.error(error);
    });
}



