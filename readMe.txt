Steps to Running (for mac):
	*If it is not installed, install node.js
	*Open Terminal
	*Run Command: sudo npm install -g http-server
	*Run Command: http-server -c-1
	*Insert localhost:8080 into adressbar on browser (8080 may be different if specified)
	*Click on main.html

I programmed this assigment using html, css, and javascript. Took me closer to 6 hours to complete.

My biggest struggle was figuring out how to get around CORS safety restrictions.

Once loading the webpage, there will be a two second wait until the elements are loaded. During the load time there is the word loading... shown.

After loading, three jobs can be seen, fetched directly from the json file provided. When hovering over each job panel, it will increase in size (by 10%). The text is sliced to 300 letters in order to match the example, with the three dots at the bottom of each.

Origionally inteded to be an "add job" feature, but ended up becoming the "duplicate jobs" feature, there is a button at the top of the screen. It will duplicate the amount of job that are on screen, just to further prove the use of fetching methods.